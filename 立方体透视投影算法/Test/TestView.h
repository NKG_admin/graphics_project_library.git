
// TestView.h : CTestView 类的接口
//

#pragma once

#include "Line.h"
#include "Face.h"
#include "P3.h"
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	void ReadPoint();//读入点表,完成函数体！
	void ReadFace();//读入面表，完成函数体！
	void InitParameter();//参数初始化，完成函数体！
	void PerProject(CP3);//透视投影，完成函数体！
	void DoubleBuffer(CDC *pDC);//双缓冲绘图
	void DrawObject(CDC *);//绘制立方体表面，完成函数体！
	virtual void OnDraw(CDC* pDC);  
	virtual void OnInitialUpdate();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP3 P[8];//点表
	CFace F[6];//面表，立方体有六个表面（前后左右上下）
	double R,Theta,Phi,d;//R,Theta,Phi视点在用户坐标系的球坐标,d视距
	double k[9];//运算常量，其中k[0]不用
	CP2 ScreenP;//屏幕坐标系的二维坐标点
	CP3 ViewPoint;
	BOOL bPlay;//动画开关
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point); //鼠标左键按下的响应函数
	afx_msg void OnPlay();//菜单响应函数
	afx_msg void OnTimer(UINT_PTR nIDEvent); //计时器响应函数
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point); //鼠标右键按下的响应函数，完成函数体！
	afx_msg void OnOnepoint();//工具条中一点透视按钮的响应函数
	afx_msg void OnTwopoint();//工具条中二点透视按钮的响应函数，完成函数体！
	afx_msg void OnThreepoint();//工具条中三点透视按钮的响应函数，完成函数体！
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

